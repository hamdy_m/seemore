from django import forms
from django.utils.translation import ugettext as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from crispy_forms.bootstrap import FormActions
from .models import NewsList


class NewsListForm(forms.ModelForm):

  class Meta:
    model = NewsList
    fields = ["name", "email"]

  @property
  def helper(self):
    helper = FormHelper()
    helper.form_method = "post"
    helper.form_action = ""
    helper.form_show_labels = False
    helper.layout = Layout(
      Field("name", placeholder=_("Name")),
      Field("email", placeholder=_("Email")),
      FormActions(
        Submit(name='submit', value=_("Sign Up"), css_class="btn btn-lg pull-right btn-signup")
      )
    )
    return helper
