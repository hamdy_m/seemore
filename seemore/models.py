from django.db import models


class NewsList(models.Model):

  name = models.CharField(max_length=64, null=False, blank=False)
  email = models.EmailField(null=False, blank=False, unique=True)

  class Meta:
    ordering = ("name",)

  def __repr__(self):
    return "<NewsList name: {}, email: {}>".format(self.name, self.email)

  def __unicode__(self):
    return "Name: {}, E-mail: {}".format(self.name, self.email)

  def __str__(self):
    return self.__unicode__()
