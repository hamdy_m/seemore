angular.module("ModalVideoApp", ["ui.bootstrap"])
    .config(function ($sceDelegateProvider) {
              $sceDelegateProvider.resourceUrlWhitelist([
                'self',
                'https://*.youtube.com/**',
                'https://*.vimeo.com/**']);
            });
angular.module("ModalVideoApp")
    .controller("ModalVideoController", ["$scope", "videoURL", function ($scope, videoURL) {
      $scope.videoURL = videoURL;
    }]).factory("VideoUtilService", ["$modal", function ($modal) {
      var service = {};
      service.getVideoURL = function (videoID) {
        if (videoID.startsWith("v::")) {
          var urlTemplate = "https://player.vimeo.com/video/";
        } else if (videoID.startsWith("y::")) {
          var urlTemplate = "https://www.youtube.com/embed/";
        }
        return urlTemplate + videoID.substring(3);
      };
      service.playVideo = function (videoID) {
        var videoURL = service.getVideoURL(videoID);
        service.videoModal = $modal.open({
          animation: true,
          templateUrl: "video-modal-template",
          controller: "ModalVideoController",
          size: "lg",
          resolve: {
            videoURL: function () {
              return videoURL;
            }
          }
        });
      };
      service.closeModal = function () {
        service.videoModal.$close();
      };
      return service;
    }]).directive("thumbnailMan", ["$http", "VideoUtilService", function ($http, VideoUtilService) {
      return {
        link: function (scope, element, attrs) {
          var videoData = attrs.videoData;  //(v,123456)
          videoData = videoData.slice(1, -1).split(',');
          var videoSource = videoData[0];
          var videoID = videoData[1];
          if (videoSource == 'v') {
            // download the vimeo thumbnail data and assign it to the src attribute
            $http.jsonp("http://vimeo.com/api/v2/video/" + videoID + ".json?callback=JSON_CALLBACK")
                .then(function (data) {
                         element.attr("src", data.data[0]["thumbnail_medium"]);
                      });
          } else if (videoSource == 'y') {
            element.attr("src", "http://img.youtube.com/vi/" + videoID + "/hqdefault.jpg");
          }
          element.on("click", function() {
            VideoUtilService.playVideo(videoSource + "::" + videoID);
          })
        }
      }
    }]);