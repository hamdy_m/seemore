import re
from django.template import Library
from django.utils import translation
from django.utils.safestring import mark_safe
from django.utils.translation.trans_real import get_languages

register = Library()

@register.simple_tag(takes_context=True)
def get_other_language_code(context):
  current_lang_code = context["request"].session.get(translation.LANGUAGE_SESSION_KEY, "en")
  languages = get_languages().copy()
  languages.pop(current_lang_code)
  return languages.popitem()[0]

@register.assignment_tag(takes_context=True)
def get_other_language_name(context):
  return get_languages().get(get_other_language_code(context))

@register.filter
def ang_inter(stmt):
  return mark_safe("{{ %s }}" % stmt)

@register.filter
def split_for_js(video_data):
  # given video_ids = "abc,cde,def,ghi"
  # returns: "'abc','cde','def','ghi'"
  ids = _data_splitter.findall(video_data)
  quote_surrounded = ["'{}'".format(id_) for id_ in ids]
  return mark_safe(','.join(quote_surrounded))

_data_splitter = re.compile(r"\(.+?\)")
