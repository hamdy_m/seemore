from django.conf.urls import include, url, patterns
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from . import views

admin.site.site_header = "Seemore Administration"
admin.site.site_title = admin.site.site_header

urlpatterns = i18n_patterns(
  url("^$", views.IndexView.as_view(), name="index"),
  url("^services/$", views.ServicesView.as_view(), name="services"),
  url("^admin/", include(admin.site.urls))
)

urlpatterns += patterns('',
  url(r'^i18n/', include('django.conf.urls.i18n'))
)
