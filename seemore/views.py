from django.views.generic import TemplateView, CreateView
from django.core.urlresolvers import reverse_lazy
from .models import NewsList
from .forms import NewsListForm


class BaseCreateView(CreateView):

  model = NewsList
  form_class = NewsListForm


class IndexView(BaseCreateView):

  template_name = "seemore/index.html"
  success_url = reverse_lazy("index")


class ServicesView(BaseCreateView):

  template_name = "seemore/services.html"
  success_url = reverse_lazy("services")